###**Description**
This repository contains the following:

- Code for aligning sequences of motifs identified in conserved non-coding elements. 
- Code for retrieving aligned subsequences. 

###**Requirements**
Compatible with Python 2.7. Requires NetworkX (1.6 or above) library.

###**License**
Copyright (C) 2016 Fabrizio Smeraldi and Maryam Abdollahyan