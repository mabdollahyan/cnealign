
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import sys


TABLEFILE='symbols_occurences.txt'

# Holds TF data in a dictionary of dicts with following entries:
# TFNAME: { 'sym': sym, 'count': count, 'length': length)
tftable=dict() # all data on TFs
sym2name=dict() # recover name


def parsetable(filename):
    # organize TF data in tftable
    # called upon module init
    global tftable
    tablefile=open(TABLEFILE, 'rt')
    # skip the header
    tablefile.readline()
    for tf in tablefile:
        #print(tf.split())
        (sym, count, tfname, dummy, length)=tf.split('\t')
        if tfname not in tftable:
            tftable[tfname]=dict()
            tftable[tfname]['count']=int(count)
            tftable[tfname]['sym']=sym
        tftable[tfname]['length']=int(length)
    
    global sym2name
    for name, rec in tftable.items():
        sym2name[rec['sym']] = name


def expand(word):
    expansion=[]
    for tf in word:
        if tf in sym2name:
            name=sym2name[tf]
        else:
            print("invalid tf in word")
            sys.exit(1)
        expansion.append("%s" % name)

    return expansion
        
        
# this code runs upon import
parsetable(TABLEFILE)
