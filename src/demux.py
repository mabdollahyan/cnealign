
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import sys 
from operator import itemgetter
import networkx as nx


STARTNODE=('START:',-10,-1)

class Writer:
    fastafile=None
    statsfile=None
    count=0
    overlaps=0
    seqname=''

    @staticmethod
    def open(fastafilename, statsfilename):
        Writer.fastafile=open(fastafilename, 'w')
        Writer.statsfile=open(statsfilename, 'w')
        Writer.statsfile.write("NAME\tTFS\tMERGED\tPATHS\n")

    @staticmethod
    def setcnename(name):
        Writer.seqname=name
        Writer.count=0
        Writer.overlaps=0

    @staticmethod
    def addoverlaps(olaps):
        Writer.overlaps+=olaps

    @staticmethod
    def close():
        Writer.fastafile.close()
        Writer.statsfile.close()

    @staticmethod
    def writefasta(path):
        Writer.count=Writer.count+1
        Writer.fastafile.write(">%s-%d\n"%(Writer.seqname, Writer.count))
        Writer.fastafile.write("%s\n"%path)

    @staticmethod
    def writestats(nmotifs):
        Writer.statsfile.write("%s\t%d\t%d\t%d\n"%(Writer.seqname, nmotifs, Writer.overlaps, Writer.count))


def compute_edges(G):
    # sort by start position
    motifs=sorted(G.nodes_iter(), key=itemgetter(1))
    
    for i in range(len(motifs)-1):
        startnode=motifs[i]
        j=i+1
        # find first non-overlapping TF
        while (motifs[j][1]<=startnode[2]): 
            j=j+1
            if j==len(motifs): break
        else: # unless we came out with a break
            leftmostend=motifs[j][2]
            # all target nodes must have start position before end position of motifs[j]...
            while motifs[j][1]<=leftmostend:
                if motifs[j][2]<=leftmostend:
                    leftmostend=motifs[j][2] # or of any other target node
                G.add_edge(startnode, motifs[j])
                j=j+1
                if j==len(motifs): break

# trace all paths starting from given node, depth first
def trace_paths(G, node, path):
    succ=G.successors(node)
    if len(succ)==0:
        Writer.writefasta(path)
    else: 
        for nxt in succ:
            trace_paths(G, nxt, path+nxt[0])

# no two nodes with same symbol can start at same position, otherwise they are the same
def merge_children (G, nodes):
    sym=(nodes[0])[0]
    newnode=(sym, 0, tuple(sorted(nodes, key=itemgetter(1))))
    if newnode not in G.nodes_iter():
        G.add_node(newnode)
        Writer.addoverlaps(len(nodes))
    for n in nodes:
        children=G.successors(n)
        for c in children:
            G.add_edge(newnode, c)
    return newnode

# remove duplicate paths
def remove_duplicates(G):
    done=False
    while not done:
        done=True
        nodes=G.nodes()
        for parent in nodes:
            if parent not in G.nodes_iter(): continue
            identical=[]
            succ=sorted(G.successors(parent), key=itemgetter(0))
            if len(succ)<=1: continue # leaf or single child
            # find first group of children with equal symbol
            currsym=''
            succ.append(('dummy', 0, 0))
            i=0
            for j in range(0, len(succ)):
                sym=(succ[j])[0]
                if sym!=currsym:
                    if j-i>1:
                        identical=succ[i:j]
                        break
                    currsym=sym
                    i=j
            if len(identical):
                # create new node with children of
                # identical nodes being merged
                newnode=merge_children(G, identical)
                # link from parent to new node
                G.add_edge(parent, newnode)
                # unlink from parent to merged nodes
                # remove if no other parents exist
                for child in identical:
                    G.remove_edge(parent, child)
                    if G.in_degree(child)==0:
                        G.remove_node(child)
                done=False
                
def build_graph(motifs):
    G=nx.DiGraph()
    startnode=STARTNODE
    G.add_node(startnode)
    G.add_nodes_from(motifs)
    compute_edges(G)
    remove_duplicates(G)
    return G

# return non-overlapping paths
def demux(motifs):
    G=build_graph(motifs)
    startnode=STARTNODE
    trace_paths(G, startnode, '')


def process(infilename, fastafilename, statsfilename):
    infile=open(infilename,'r')
    Writer.open(fastafilename, statsfilename)

    cnename=''
    cnemotifs=[]

    next(infile) # skip the header
    for line in infile:
        (name,symbol,start,stop)= line.expandtabs(1).split()[1:5]
        # new: process old one and reset
        if name!=cnename:
            if len(cnemotifs):
                demux(cnemotifs)
                Writer.writestats(len(cnemotifs))
                cnemotifs=[]
            cnename=name
            Writer.setcnename(cnename)
        # current: append new TF as a tuple 
        cnemotifs.append((symbol,int(start),int(stop)))
    # last one
    demux(cnemotifs)
    Writer.writestats(len(cnemotifs))
        
    infile.close()
    Writer.close()


if __name__ == '__main__':
    if len(sys.argv)<3:
        print("Usage: demux.py <infile> <fastafile> <statsfile>")
    else:
        process(*sys.argv[1:])
        