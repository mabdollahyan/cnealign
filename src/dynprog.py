
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import sys
import networkx as nx
import topwords.demux as dmx


class Scores: # alignment parameters
    start=0 
    gap=-1
    #f=open('scores.txt', 'r')
    #rows=(line.split('\t') for line in f)
    #match={row[0].strip('\t\n\r'):float(row[1].strip('\t\n\r')) for row in rows} 
    match=1
    mismatch=0
    
class Aln: # alignment types
    Global=0
    Semi=1 
    
class RefGraph: # reference graph, forward and reverse
    fwd=None
    rev=None


def leaves(G):
    return [node for node in G.nodes_iter() if G.out_degree(node)==0]

def setzeroscore(G, alntype):
    if alntype==Aln.Semi:
        for node in G.nodes_iter():
            if ((node[0]==dmx.STARTNODE) or (node[1]==dmx.STARTNODE)):
                nx.set_node_attributes(G,'score', {node: Scores.start})
    else:
        # for global just set root node
        nx.set_node_attributes(G,'score', 
                               {(dmx.STARTNODE,dmx.STARTNODE): Scores.start})


def edge_score(pred, node):
    if (pred[0]==node[0]) or (pred[1]==node[1]):
        return Scores.gap 
    elif node[0][0]==node[1][0]:
        #return Scores.match[node[0][0]]
        return Scores.match
    else:
        return Scores.mismatch

    
def dynprog(G, alntype):
    # dynamic programming on graph G - use stack
    # to recurse through graph until ground case
    setzeroscore(G, alntype)              
    stack=leaves(G)
    while len(stack)>0:
        node=stack[-1]
        if 'score' in G.node[node]: # already processed
            stack.pop()
            continue
        preds=G.predecessors(node)
        complete=True
        scores=[]
        for p in preds:
            if 'score' not in G.node[p]: # if predecessor has not been processed yet add to queue
                stack.append(p)
                complete=False
            elif complete: # else compute score
                pathscore=G.node[p]['score']+edge_score(p,node)
                scores.append(pathscore)
        # if all predecessors have been processed, compute score for node
        # keep only paths with max score
        if complete:
            optimum=max(scores)
            for p,s in zip(preds, scores):
                if s<optimum:
                    G.remove_edge(p, node)
            nx.set_node_attributes(G,'score', {node: optimum})
            stack.pop() # node has been processed

    return G


# return list of top scoring nodes
def topnodes(G, alntype, alnLeaves, graphA, graphB):
    maxscore=float("-inf")
    maxnodes=[]
    if alntype==Aln.Global:
        nodelist=alnLeaves
    else: 
        leavesA=leaves(graphA)
        leavesB=leaves(graphB)
        nodelist=[n for n in G.nodes_iter() if ((n[0] in leavesA) or (n[1] in leavesB))]
    for i in nodelist:
        if G.node[i]['score']>maxscore:
            maxscore=G.node[i]['score']
            maxnodes=[i]
        elif G.node[i]['score']==maxscore:
            maxnodes.append(i)

    return (maxnodes, maxscore)


# trace back from top nodes to root node
# yield all possible paths
def traceback_gen (G, endpoint, alntype):
    if ((alntype==Aln.Global) or 
       ((alntype==Aln.Semi) and ((endpoint[0]!=dmx.STARTNODE) and (endpoint[1]!=dmx.STARTNODE)))):
        preds=G.predecessors(endpoint)
    else:
        preds=[]

    if len(preds):
        for n in preds:
            for path in traceback_gen(G, n, alntype):
                path.append(endpoint)
                yield path
    else:
        yield [endpoint]


def reverse(G):
    # reverse all edges and move start node accordingly
    # to point to leaves of forward graph
    H=G.copy()
    leafnodes=leaves(H)
    # remove edges from start node
    H.remove_edges_from(H.edges([dmx.STARTNODE]))
    # reverse edges in place
    H.reverse(copy=False)
    # create edges from start node to former leaves
    for leaf in leafnodes:
        H.add_edge(dmx.STARTNODE, leaf)
    return H


def setreference(seq):
    RefGraph.fwd=dmx.build_graph(seq)
    RefGraph.rev=reverse(RefGraph.fwd)


# convert paths into alignments
def getalignment(G, path):
    cne1=''
    cne2=''
    node=path[0]  
    oldnode1=node[0]
    oldnode2=node[1]
    for node in path[1:]:
        node1=node[0]
        node2=node[1]
        if oldnode1==node1:
            cne1=cne1+'-'
            cne2=cne2+node2[0]
        elif oldnode2==node2:
            cne1=cne1+node1[0]
            cne2=cne2+'-'
        else:
            cne1=cne1+node1[0]
            cne2=cne2+node2[0]
        oldnode1=node1
        oldnode2=node2
    return [cne1,cne2]


def align(seqB, alntype, listall):
    graphB=dmx.build_graph(seqB)
    dynGraphs=[]
    endnodeslist=[]
    scores=[]
    for graphA in [RefGraph.fwd, RefGraph.rev]: 
        alnGraph=nx.strong_product(graphA, graphB)
        # for global alignment, keep track of leaves of search space graph
        alnLeaves=leaves(alnGraph) if alntype==Aln.Global else None
        G=dynprog(alnGraph, alntype) # modifies the graph in place
        # pass on leaves from original graph before pruning
        (endnodes, score)=topnodes(G, alntype, alnLeaves, graphA, graphB)
        dynGraphs.append(G)
        endnodeslist.append(endnodes)
        scores.append(score)
    # generate alignments with max score
    alignments=[]
    maxscore=max(scores)
    altscore=min(scores)
    for dd in [0,1]:
        if scores[dd] < maxscore: continue
        endnodes=endnodeslist[dd]
        G=dynGraphs[dd]
        
        patterns=[]
        while len(endnodes):
            node=endnodes.pop()
            for path in traceback_gen(G, node, alntype):
                aln=getalignment(G, path)
                ast, pat=asterisks(aln)
                if pat not in patterns: # unique matching pattern
                    patterns.append(pat)
                    aln.append(ast)
                    aln.append('+' if dd==0 else '-')
                    alignments.append(aln)
                if not listall:  break 
            if not listall:  break 
        if not listall:  break 
    return (alignments, maxscore, altscore)


def readcnes(infilename):
    infile=open(infilename,'r')
    cnelist=[]
    cnename=''
    cnemotifs=[]
    
    next(infile) # skip the header
    for line in infile:
        (name,symbol,start,stop)= line.expandtabs(1).split()[1:5]
        # new: process old one and reset
        if name!=cnename:
            if len(cnemotifs):
                cnelist.append(cnemotifs)
                cnemotifs=[]
            cnename=name
        # current: append new motif as a tuple 
        cnemotifs.append((symbol,int(start),int(stop)))
    # last one 
    cnelist.append(cnemotifs)
    infile.close()
    return cnelist


# put asterisks below matching letters
def asterisks(aln):
    m=''
    p=''
    for x,y in zip(aln[0],aln[1]):
        if x==y:
            m=m+'*'
            p=p+x
        else:
            m=m+' '
    return m,p
    

def process (infilename, outfilename):
    cnelist=readcnes(infilename)
    setreference(cnelist[0])
    alns, score, altscore=align(cnelist[1], alntype=Aln.Global, listall=True)
    outf=open(outfilename, 'w')
    outf.write("Score: %f (Other direction: %f)\n" % (score, altscore))
    for al in alns:
        outf.write("%s    %c\n" % (al[0], al[3]))
        outf.write("%s    +\n" % al[1])
        outf.write("%s\n" % al[2])
    outf.close()


if __name__ == '__main__':
    if len(sys.argv)<2:
        print("Usage: dynprog.py <infile> <outfile>")
    else:
        process(*sys.argv[1:])
