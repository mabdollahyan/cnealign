
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import sys
import os
import topwords.dynprog as dyn


# return all sequences from given sequence name OR file position
def readcnes(infilename, startline=0, startcne=''): 
    infile=open(infilename,'r')
    if startline>0:
        startcne=''
        for _ in range(startline):
            next(infile)
    endline=startline
    cnename=''
    cnemotifs=[]
    for line in infile:
        # discard comments
        if line[0]=='#': 
            endline+=1
            continue
        (name,symbol,start,stop)= line.expandtabs(1).split()[1:5]
        # discard all before startcne
        if startcne!='':
            if name!=startcne:
                endline+=1
                continue
            else:
                startcne=''
        # new: process old one and reset
        if name!=cnename:
            if len(cnemotifs):
                yield (cnename, cnemotifs, endline)
                cnemotifs=[]
            cnename=name
        # current: append new motif as a tuple 
        cnemotifs.append((symbol,int(start),int(stop)))
        endline+=1
    # last one 
    infile.close()
    yield (cnename, cnemotifs, endline)


def process (infilename, outdir, atype='g', howmany='b', startcne=''):
    if not os.path.isdir(outdir):
        os.mkdir(outdir)
    alnopts={'g': dyn.Aln.Global, 's': dyn.Aln.Semi}
    alntype=alnopts[atype];
    if howmany=='a':
        listall=True
    else:
        listall=False

    endline=0
    while True:
        startline=endline # start from next one as reference
        cnegenerator=readcnes(infilename, startline, startcne)
        cne1=next(cnegenerator)
        (name1, motifs1, endline)=cne1
        outfname=os.path.join(outdir, name1.split(':')[0]+'.aln')
        outf=open(outfname,'w')

        # align cne1 with all remaining sequences
        dyn.setreference(motifs1)
        count=0
        for cne2 in cnegenerator:
            count+=1
            (name2, motifs2, dummy)=cne2                
            alns,score,altscore=dyn.align(motifs2,alntype,listall)
            header=">\t%s\t%s\t%d\t(%d)" % (name1,name2,score,altscore)
            print(header)
            for al in alns:
                outf.write(header+"\t%c\n" % al[3]) # add alignment direction to header
                outf.write("%s\n" % al[0]) # cne1
                outf.write("%s\n" % al[1]) # cne2
                outf.write("%s\n" % al[2]) # asterisks
        outf.close()
        if count==1: # we are down to last but one and last one
            break


if __name__ == '__main__':
    if len(sys.argv)<2:
        print("Usage: align.py <infile> <outdir> [<g|s> <a|b> <startcne>]")
        print("       defaults: <g>lobal, <b>est, first CNE")
        print("       (reference CNE is reversed for rev alignment)\n")
    else:
        process(*sys.argv[1:])
