
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import sys
import topwords.symboltable as sym
import topwords.alnreader as ar


def segment(aln, minlength):
    # find all words of minlength in the alignment
    words=[]
    ast=aln.getAsterisks()
    stars=ast.split()
    start=0
    cne=aln.getSequence(aln.refCNE)
    for s in stars:
        # position of the series of stars
        pos=ast.find(s, start)
        start=pos+len(s)
        if len(s)<int(minlength):
            continue
        w=cne[pos:pos+len(s)]
        words.append(w)

    return words


def getsubstrings(string):
    length = len(string)
    return [string[i:j+1] for i in range(length) for j in range(i,length)]


def countwords(minlength, alntype):
    # count unique words that are present in alignment
    freqtable=dict()
    for fileno in ar.CNEFileNo(alntype):
        print("Reading ", fileno)
        for aln in ar.AlnFile(fileno, alntype):
            for w in segment(aln, minlength):
                for s in getsubstrings(w):
                    varw=[s, s[::-1]]
                    for k in varw:
                        if k in freqtable:
                            freqtable[k]=freqtable[k]+1
                            break
                    else:
                        freqtable[s]=1
    return freqtable
                

def process(alntype, outfname, minlength=3, mintimes=10):
    freqtable=countwords(minlength, alntype)
    wlist=list(freqtable.items())
    # sort first by frequency
    wlist.sort(key=lambda x: x[1], reverse=True)
    # and then by word length
    wlist.sort(key=lambda x: len(x[0]), reverse=True)
    out=open(outfname, 'wt')
    out.write("WORD\tCOUNT\tLENGTH\tMOTIFS\n")
    count=0
    wlen=0
    for w in wlist:
        if len(w[0])!= wlen:
            wlen=len(w[0])
            count=0
        # skip words that occur less than a mintimes
        if w[1]<int(mintimes): continue
        line="%s\t%d\t" % w
        line=line+str(wlen)+'\t'
        for tf in sym.expand(w[0]):
            line=line+tf+" "
        out.write(line+'\n')
        count=count+1
        
    out.close()


if __name__ == '__main__': 
    if len(sys.argv)<2:
        print("Usage: wordstats.py alntype <outfile> [<minlength> <mintimes>]")
        print("Remember to choose symbols.txt as appropriate")
    else:
        process(*sys.argv[1:])
        