
# (C) Fabrizio Smeraldi and Maryam Abdollahyan 2016

import os.path
import sys
import re
import shutil


CONFIGFILE='alnreader.cfg'
LISTFILE='CNEnumbers.lst'
CNEFILEFMT='CRCNE%08d.aln'

# contains paths to the various alignment types
# as read from the config file
AlnPaths=dict()

# holds data about an alignment
class AlnObj:
    # header regexp, precompiled - handle multiple concatenated CNE 
    # numbers by picking first only
    regexp=re.compile('\>\s+CRCNE(\d+)[:CRCNE0-9]*\s+CRCNE(\d+)[:CRCNE0-9]*'
                      '\s+(\-*\d+)\s+\(\s*(\-*\d+)\)\s+([+-])')

    def __init__(self, header, first, second, stars):
        # match header fields       
        hdr=AlnObj.regexp.match(header)
        if hdr==None:
            print("could not match")
            print(header)
            exit(1)
        self.refCNE=int(hdr.group(1))
        self.altCNE=int(hdr.group(2))
        self.score=float(hdr.group(3))
        self.altscore=float(hdr.group(4))
        polarity=hdr.group(5)
        self.seq={self.refCNE: first.rstrip('\n'), 
                  self.altCNE: second.rstrip('\n')}
        if polarity=='-':
            self.bkwds={self.refCNE: True, self.altCNE: False}
        else:
            self.bkwds={self.refCNE: False, self.altCNE: False}
        self.ast=stars.rstrip('\n')
        # consistency check - ast should have all the spaces
        if ((len(self.ast)!=len(self.seq[self.refCNE])) or
            (len(self.ast)!=len(self.seq[self.altCNE])) ):
            print("length mismatch creating AlnObj")
            exit(1)

    def isBackwards(self, cne=None):
        # return True if either the specified CNE or the alignment
        # is backwards
        if cne!=None:
            return self.bkwds[cne]
        val=self.bkwds.values()
        return not (val[0] or val[1])

    def flip(self):
        # flip the strings in the alignment
        for k,v in self.seq.items():
            self.seq[k]=v[::-1]
            self.bkwds[k]=not self.bkwds[k]
        self.ast=self.ast[::-1]

    def setRefCNE(self, cneno):
        # set reference CNE for printing
        if self.altCNE==cneno:
            self.refCNE, self.altCNE= self.altCNE, self.refCNE
        elif self.refCNE!=cneno:
            print("wrong reference CNE: %d" % cneno)
            exit(1)
        if self.bkwds[self.refCNE]:
            self.flip()

    def getAsterisks(self):
        return self.ast

    def getSequence(self, cneno):
        return self.seq[cneno]

    def __str__(self):
        # convert to string
        s="> CNE%d " % self.refCNE
        if self.isBackwards(self.refCNE):
            s+="(Bak)"
        else:
            s+="(Fwd)"
        s+="   CNE%d " % self.altCNE
        if self.isBackwards(self.altCNE):
            s+="(Bak)"
        else:
            s+="(Fwd)"
        s+="   Score %6.2f   AltScore %6.2f\n" % (self.score, self.altscore)
        s+=self.seq[self.refCNE]+"\n"
        s+=self.seq[self.altCNE]+"\n"
        s+=self.ast 
        return s


# return the full path to CNE file with 
# given number
def CNEFileName(cneno, alntype='global'):
    cnepath=AlnPaths[alntype]
    name=CNEFILEFMT%cneno
    return os.path.join(cnepath, name)


# generator that returns all file numbers for a particular
# alignment type - call AlnFile on each of these to get all
# the alignments
def CNEFileNo(alntype='global'):
    cnepath=AlnPaths[alntype]
    fname=os.path.join(cnepath, LISTFILE)
    for line in open(fname,'rt'):
        yield (int(line))


# generator function returns all alignments from a given file
def AlnFile(cneno, alntype='global'):
    fname=CNEFileName(cneno,alntype)
    infile=open(fname,'rt')
    for header in infile:
        if header[0]!='>': continue
        first=next(infile)
        second=next(infile)
        stars=next(infile)
        yield AlnObj(header, first, second, stars)


# return a specific alignment given the numbers of the CNEs
def GetAln(cne1, cne2, alntype='global'):
    ref=min(cne1,cne2)
    alt=max(cne1,cne2)
    for aln in AlnFile(ref, alntype):
        if alt in aln.seq.keys():
            aln.setRefCNE(cne1)
            return aln
    return None


# generator function returning all alignments with a given
# CNE, that is taken as the reference
def AlnSlice(ref, alntype='global'):
    # first list CNEs with lower number - this is slow
    for alt in CNEFileNo(alntype):
        if alt==ref: break
        aln=GetAln(ref, alt, alntype)
        yield aln
    # then CNEs with higher number. Flip to keep reference unchanged
    for aln in AlnFile(ref, alntype):
        aln.setRefCNE(ref)
        yield aln
    

# list all CNE files under a given path. Note that the
# last CNE has no file and therefore is not listed
def ListCNEs(cnepath, alntype):
    # get all file names and extract CNE numbers
    directory=os.listdir(cnepath)
    cnes=[]
    for filename in directory:
        (name, ext)=os.path.splitext(filename)
        if ext!='.aln': continue
        # extract  the number of the first CNE in the filename, in the case
        # of names like CRCNE00000001:CRCNE00000002.aln
        components=name.split(':')
        cneno=int(components[0][-8:])
        cnes.append(cneno)
        if len(components)>1:
            # create link to file under name of 1st CNE only
            stdname=CNEFileName(cneno, alntype)
            if not os.path.isfile(stdname):
                try:
                    os.symlink(filename, stdname)
                except AttributeError:
                    shutil.copyfile(os.path.join(cnepath ,filename), os.path.join(cnepath, stdname)) 
    cnes.sort()
    # save all to file
    listfname=os.path.join(cnepath,LISTFILE)
    outfile=open(listfname, 'wt')
    outfile.writelines([("%d\n"%x) for x in cnes])
    outfile.close()

    
# ------- setup code - runs upon import ---------

# set up paths
if not os.path.isfile(CONFIGFILE):
    print("could not find %s" % CONFIGFILE)
    sys.exit(1)
for line in open(CONFIGFILE, 'rt'):
    k, p = line.split()
    AlnPaths[k]=p

# create list of CNEs if not present
for (alntype, cnepath) in AlnPaths.items():
    filename=os.path.join(cnepath, LISTFILE)
    if os.path.isfile(filename): continue
    print("creating %s" % filename)
    ListCNEs(cnepath, alntype)
